-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2019 at 07:49 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR001', 'leptop asus i5', 800000, 'JN002', 1, 110),
('BR002', 'hp oppo f5', 70000, 'JN001', 1, 27),
('BR003', 'buku tulis', 360000, 'JN004', 1, 18),
('BR004', 'pulpen', 4000, 'JN001', 1, 5),
('BR005', 'leptop', 200000, 'JN001', 1, 3),
('BR006', 'pensil', 2000, 'JN003', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'ob', 'Ob g', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'admin', 'Opersional', 1),
('JB004', 'Manager', 'Admin', 1),
('JB006', 'DIREKTUR', '"DIREKTUR', 1),
('JB007', 'MANAGER', '"MANAGER', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat tulis ', 1),
('JN002', 'Perangkat keras', 1),
('JN003', 'alat tulis', 1),
('JN004', 'tangga', 1),
('JN005', 'rumah', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`) VALUES
('134562578', 'tari', 'bandung', '1970-10-10', 'p', '"""""jl.kesemek no 90"""""', '08655443335', 'JB006', 1),
('166777777', 'ira', 'bandung', '1959-02-01', 'p', '"jl.lima maret"', '08655443335', 'JB002', 1),
('1704421300', 'sebastian', 'cirebon', '1963-03-04', 'L', 'jl.delima no 78', '09765546777', 'JB003', 1),
('19024', 'dafi', 'kuningan', '1962-04-04', 'L', 'jjjj', '0488888', 'JB004', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 1, 'BR001', 10, 50000, 500000, 1),
(2, 0, 'BR001', 10, 50000, 500000, 1),
(3, 0, 'BR001', 12, 3400000, 40800000, 1),
(4, 0, 'BR001', 12, 3400000, 40800000, 1),
(5, 0, 'BR001', 12, 3400000, 40800000, 1),
(6, 0, 'BR001', 12, 3400000, 40800000, 1),
(7, 0, 'BR002', 5, 50000, 250000, 1),
(8, 0, 'BR002', 5, 50000, 250000, 1),
(9, 0, 'BR002', 5, 50000, 250000, 1),
(10, 0, 'BR002', 5, 50000, 250000, 1),
(11, 5, 'BR001', 10, 50000, 500000, 1),
(12, 1, 'BR003', 12, 1200000, 14400000, 1),
(13, 1, 'BR001', 2, 8000000, 16000000, 1),
(14, 9, 'BR002', 7, 50000, 350000, 1),
(15, 9, 'BR003', 3, 150000, 150003, 1),
(16, 9, 'BR003', 3, 150000, 150003, 1),
(17, 10, 'BR001', 6, 600, 606, 1),
(18, 10, 'BR001', 6, 600, 606, 1),
(19, 10, 'BR001', 6, 600, 606, 1),
(20, 10, 'BR001', 6, 600, 606, 1),
(21, 10, 'BR001', 6, 600, 606, 1),
(22, 11, 'BR005', 3, 1000000, 1000000, 1),
(23, 11, 'BR004', 5, 1000, 1005, 1),
(24, 2, 'BR004', 2, 50000, 50002, 1),
(25, 2, 'BR002', 10, 1200000, 1200010, 1),
(26, 2, 'BR005', 1, 10000000, 10000000, 1),
(27, 15, 'BR005', 9, 600000, 600009, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tanggal`, `kode_supplier`, `approved`, `flag`) VALUES
(1, 'trp0988', '2019-02-14', 'SP001', 1, 1),
(2, '1234', '2019-02-19', 'SP002', 1, 1),
(3, '1234', '2019-02-19', 'SP002', 1, 1),
(4, '1234', '2019-02-19', 'SP002', 1, 1),
(5, '1234', '2019-02-19', 'SP002', 1, 1),
(6, 'SP008', '2019-02-19', 'SP003', 1, 1),
(7, 'SP008', '2019-02-19', 'SP001', 1, 1),
(8, 'SP008', '2019-02-19', 'SP001', 1, 1),
(9, '2333', '2019-02-20', 'SP003', 1, 1),
(10, '4555', '2019-02-20', 'SP001', 1, 1),
(11, '78', '2019-02-21', 'SP002', 1, 1),
(12, '1340', '2019-03-23', 'SP003', 1, 1),
(13, '897', '2019-03-23', 'SP003', 1, 1),
(14, '790', '2019-03-23', 'SP001', 1, 1),
(15, '6777', '2019-03-25', 'SP001', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'Tania', 'jl.semangka raya no.56"', '087226246744', 1),
('SP002', 'CV.iing', 'jl.raya no 23"', '088866666666', 1),
('SP003', 'gilang', 'Jl.elang raya no.89"', '0215689389966', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '134562578', 'user@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1),
(2, '166777777', 'admin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
